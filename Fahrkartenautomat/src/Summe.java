import java.util.Scanner;

public class Summe {

	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {

		int zahl;
		int summe = 0;

		System.out.println("Geben Sie bitte einen begrenzenden Wert ein: ");
		int begrenzteZahl = tastatur.nextInt();

		System.out.print("\n\n");

		// -------------

		for (zahl = 1; zahl <= begrenzteZahl; zahl++) {
			summe = summe + zahl;
			System.out.println(summe);

		}

	}

}
