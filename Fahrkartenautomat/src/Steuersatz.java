import java.util.Scanner;

public class Steuersatz {

	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {

		double grundbetrag;
		char steuersatz;

		while (true) {
			System.out.print("Geben Sie hier den Nettowert ein:  ");
			grundbetrag = tastatur.nextDouble();

			System.out.print("Soll der erm��igte (E) oder der volle\r\n" + "Steuersatz (V) angewendet werden? ");
			steuersatz = tastatur.next().toUpperCase().charAt(0);

			if (steuersatz == 'E') {
				grundbetrag = grundbetrag * 0.93;
				System.out.println("Der neue Preis betr�gt: " + grundbetrag);
				break;
			} else if (steuersatz == 'V') {
				grundbetrag = grundbetrag * 0.81;
				System.out.println("Der neue Preis betr�gt: " + grundbetrag);
				break;
			}
		}

	}

}
