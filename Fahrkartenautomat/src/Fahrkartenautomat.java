﻿import java.util.Scanner;

class Fahrkartenautomat {

	// Bestellung erfassen
	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets;
		double ticketPreis = 0.0;
		Scanner tastatur = new Scanner(System.in);
		int auswahl;
		String[] bezeichnung = new String [] {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		double[] preis = new double [] {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		
		
		
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("===========================\n");
		System.out.println("Waehlen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		for(int i = 0; i < bezeichnung.length; i++){
			System.out.println(bezeichnung[i] + " - Preis: " + preis[i] + " Fahrkartenummer: " + i);
		}
		
		do {
			System.out.print("Ihre Wahl:");
			auswahl = tastatur.nextInt();
		
			
			
		} while (auswahl < 0 || auswahl > 9);

		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();
		ticketPreis = preis[auswahl];
		
		if (ticketPreis <= 0) {
			ticketPreis = 1;
		}

		if (anzahlTickets < 1 || anzahlTickets > 10) {
			anzahlTickets = 1;
		}

		return ticketPreis * anzahlTickets;
	}

	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	//Fahrschein wird ausgegeben
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 3; i++) {
			System.out.print("-> -> ");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der Rückgabebetrag in Höhe von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-MÃ¼zen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;

		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
		} while (true);
		
	}
}