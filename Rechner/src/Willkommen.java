import java.util.Scanner; // Import der Klasse Scanner 
 
public class Willkommen  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.println("WILKOMMEN");
    System.out.println("\n");
    
    
    System.out.println("Bitte geben Sie Ihren Namen ein: ");    
     
    // Die Variable name speichert die erste Eingabe 
    String name = myScanner.next();  
     
    System.out.print("Wie ist Ihr Alter: "); 
     
    // Die Variable alter speichert die zweite Eingabe 
    String alter = myScanner.next();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    System.out.print("\nIhr Name ist " + name + " und Sie sind " + alter + " Jahre alt.");
 
    myScanner.close(); 
     
  }    
  
  
}

