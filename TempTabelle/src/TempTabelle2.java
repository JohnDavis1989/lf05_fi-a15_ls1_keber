
public class TempTabelle2 {

	public static void main(String[] args) {
		
		// Temperaturtabelle
		//----------------------------------------------------
		String f = "Fahrenheit |";
		String c = "Celsius";
		System.out.printf(  f  );
		System.out.printf( "%10s\n", c);
		System.out.printf( "----------------------\n");
		
		//----------------------------------------------------
		String strich = "|";
		//----------------------------------------------------
		
		double f1 = -20;
		System.out.printf("%.0f", f1);
		System.out.printf("%9s", strich);
		double c1 = -28.8889;
		System.out.printf("%10.2f\n", c1);
		
		//----------------------------------------------------
		
		double f2 = -10;
		System.out.printf("%.0f", f2);
		System.out.printf("%9s", strich);
		double c2 = -23.3333;
		System.out.printf("%10.2f\n", c2);
		
		//----------------------------------------------------
		
		double f3 = +0;
		System.out.printf("+%.0f", f3);
		System.out.printf("%10s", strich);
		double c3 = -17.7778;
		System.out.printf("%10.2f\n", c3);
		
		//----------------------------------------------------
		
		double f4 = +20;
		System.out.printf("+%.0f", f4);
		System.out.printf("%9s", strich);
		double c4 = -6.6667;
		System.out.printf("%10.2f\n", c4);
		
		//----------------------------------------------------
		
		double f5 =  +30 ;
		System.out.printf("+%.0f", f5);
		System.out.printf("%9s", strich);
		double c5 = -1.1111;
		System.out.printf("%10.2f\n", c5);
		
		//----------------------------------------------------
	}

}
