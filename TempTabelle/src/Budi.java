
public class Budi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.printf( "%-5s = %-18s = %4d\n", "0!", "",  					1);
		System.out.printf( "%-5s = %-18s = %4d\n", "1!", "1",  					1);
		System.out.printf( "%-5s = %-18s = %4d\n", "2!", "1 * 2",  				2);
		System.out.printf( "%-5s = %-18s = %4d\n", "3!", "1 * 2 * 3",  			6);
		System.out.printf( "%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4",  		24);
		System.out.printf( "%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5",  	120);
		// Möglichkeit 1  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		
		System.out.printf( "\n");
		System.out.println( "- - - - - - - - - - - - - - - - -\n");
		System.out.printf( "\n");
		// Platzhalter  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		
		System.out.printf( "%-5s = %-18s = %4d\n", "0!", "", 					1);
		System.out.printf( "%-5s = %-18s = %4d\n", "1!", "1", 					1);
		System.out.printf( "%-5s = %-18s = %4d\n", "2!", "1 * 2", 				1*2);
		System.out.printf( "%-5s = %-18s = %4d\n", "3!", "1 * 2 * 3", 			1*2*3);
		System.out.printf( "%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4", 		1*2*3*4);
		System.out.printf( "%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 	1*2*3*4*5);
		// Möglichkeit - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		
	}

}
