
public class Budo {

	public static void main(String[] args) {
		String a = "0!";
		// 0! 
		
		String b = "1!"; 
		// 1!
		
		String c = "2!"; 
		// 2! 
		
		String d = "3!"; 
		// 3!
		
		String e = "4!"; 
		// 4!
		
		String f = "5!"; 
		// 5!
		
		String g = "="; 
		// =
		
		String h = "0";
		// 0
		
		String i = "1"; 
		// 1
		
		String j = "2"; 
		// 2 
		
		String k = "3"; 
		// 3
		
		String l = "4"; 
		// 4
		
		String m = "5"; 
		// 5
		
		String n = "6"; 
		// 6
		
		String o = "24"; 
		// 24
		
		String p = "120"; 
		// 120
		
		String q = "*"; 
		// *
		
		System.out.printf( "%1s", a );
		System.out.printf( "%5s", g );
		System.out.printf( "%4s", i );
		System.out.printf( "%15s", g );
		System.out.printf( "%5s \n", i );
		
		System.out.printf( "%1s", b );
		System.out.printf( "%5s", g );
		System.out.printf( "%4s", i );
		System.out.printf( "%15s", g );
		System.out.printf( "%5s \n", j );
		
		System.out.printf( "%1s", c );
		System.out.printf( "%5s", g );
		System.out.printf( "%19s", g );
		System.out.printf( "%5s \n", k );
		
		System.out.printf( "%1s", d );
		System.out.printf( "%5s", g );
		System.out.printf( "%19s", g );
		System.out.printf( "%5s \n", k );
		
		System.out.printf( "%1s", e );
		System.out.printf( "%5s", g );
		System.out.printf( "%19s", g );
		System.out.printf( "%5s \n", l );
		
		System.out.printf( "%1s", f );
		System.out.printf( "%5s", g );
		System.out.printf( "%19s", g );
		System.out.printf( "%5s \n", m );

	}

}
